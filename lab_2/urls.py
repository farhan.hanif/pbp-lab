from django.urls import path
from .views import json, xml, index

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json)
]