// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, prefer_final_fields

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String title = 'Tasks';
  const MyApp({Key? key}) : super(key: key);
  @override
  MyApp createState() => MyApp();
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.grey),
        home: MyHomePage(title: title),
      );
}

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({
    required this.title,
  });
    @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  int MyHomePageStateIndex = 0;

  static final _formKey = GlobalKey<FormState>();
  static String nama = "";
  static String description = "";
  static String due_date = "";

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    DataTable(  
      columns: [  
        DataColumn(label: Text(  
            'Name',  
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
        )),  
        DataColumn(label: Text(  
            'Description',  
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
        )),  
        DataColumn(label: Text(  
            'Due Date',  
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
        )), 
        DataColumn(label: Text(  
            'Action',  
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)  
        )), 
      ],  
      rows: [  
        DataRow(cells: [  
          DataCell(Text('Lab 6 SDA')),  
          DataCell(Text('Implementasi Heap Tree')),  
          DataCell(Text('November, 16, 2021')),
          DataCell(TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
            ),
            onPressed: () { },
            child: Text('Done'),
          )
          )  
        ]),  
        DataRow(cells: [  
          DataCell(Text('Mini research MPPI')),  
          DataCell(Text('Bagian literatur review')),  
          DataCell(Text('November, 26, 2021')),
          DataCell(TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.red),
            ),
            onPressed: () { },
            child: Text('Done'),
          )
          )    
        ]),    
      ],  
    ), 
    Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            autofocus: true,
            keyboardType: TextInputType.url,
            maxLength: 30,
            decoration: InputDecoration(
                hintText: "Enter a Name",
                contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                )),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please Insert a Name';
              }
              nama = value;
              return null;
            },
          ),
          TextFormField(
            autofocus: true,
            keyboardType: TextInputType.url,
            maxLength: 30,
            decoration: InputDecoration(
                hintText: "Enter a Description",
                contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                )),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please Insert a Description';
              }
              description = value;
              return null;
            },
          ),
          TextFormField(
            autofocus: true,
            keyboardType: TextInputType.url,
            maxLength: 30,
            decoration: InputDecoration(
                hintText: "Enter the Due Date DD-MM-YYYY",
                contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                )),
            validator: (value) {
              if (value!.isEmpty) {
                return 'Please Insert the Due Date';
              }
              due_date = value;
              return null;
            },
          ),
          SizedBox(
            height: 12,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              textStyle: const TextStyle(fontSize: 16),
              primary: Colors.orange.shade800,
              onPrimary: Colors.white,
              side: BorderSide(width: 2, color: Colors.transparent),
              padding: EdgeInsets.only(
                  left: 12, right: 12, top: 8, bottom: 8),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(18.0)),
            ),
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                print(nama + " with deadline " + due_date + " successfully added");
              }
            },
            child: const Text('Add'),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      MyHomePageStateIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: ListView(children: <Widget>[ 
        _widgetOptions.elementAt(MyHomePageStateIndex), 
          ]),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment_outlined),
            label: 'Add Task',
          ),
        ],
        currentIndex: MyHomePageStateIndex,
        selectedItemColor: Colors.amber[800],
        onTap: (index) {
            setState(() {
              MyHomePageStateIndex = index;
            });
          },
      ),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.grey,
              ),
              child: Text('Menu'),
            ),
            ListTile(
              title: const Text('Tasks'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Personal Journal'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Notes'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Quotes'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Weekly Schedule'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Profile'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget form() {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: ListView(children: <Widget>[
        TextFormField(
          autofocus: true,
          decoration: InputDecoration(
              hintText: "Enter a Name",
              contentPadding: EdgeInsets.fromLTRB(12, 0, 0, 0),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              )),
          validator: (value) {
            if (value!.isEmpty) {
              return 'Please Insert a Name';
            }
            return null;
          },
        ),
      ]
      )
    );
  }
}