1) Secara umum, XML merupakan sebuah bahasa mark up, sedangkan JSON adalah format yang digunakan untuk mempresentasikan data dengan terstruktur dan lebih readable. Untuk lebih detailnya berikut beberapa perbedaan XML dan JSON:
    a. XML menyediakan dukungan untuk namespace sedangkan JSON tidak
    b. JSON mendukung untuk menggunakan array sedangkan XML tidak
    c. XML memiliki tingkat keamanan yang lebih baik dibandingkan JSON

2) Secara umum, HTML digunakan untuk menampilkan data dan membuat struktur pada sebuah halaman web, sedangkan XML hanya digunakan untuk menyimpan dan mentransfer data

Referensi:
https://www.geeksforgeeks.org/difference-between-json-and-xml/