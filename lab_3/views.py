from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from .forms import FriendForm
from .models import Friend
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if (form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3')
    else:
        response = {'form': form}
        return render(request, 'lab3_form.html', response)