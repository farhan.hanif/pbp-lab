from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    note = Note.objects.all()
    response = {'Note': note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        response = {'form': form}
        return render(request, 'lab4_form.html', response)

def note_list(request):
    note = Note.objects.all()
    response = {'Note': note}
    return render(request, 'lab4_note_list.html', response)